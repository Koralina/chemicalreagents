package com.chem.chemistry.model;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Method {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long methodId;
    private String methodName;
    @ManyToMany
    @JoinTable(name = "methods_reagents",
            joinColumns = @JoinColumn(name = "method_id",
                    referencedColumnName = "methodId"),
            inverseJoinColumns = @JoinColumn(name = "reagent_id",
                    referencedColumnName = "reagentId"))
    private Set<Reagent> reagents;

    public Set<Reagent> getReagents() {
        return reagents;
    }

    public void setReagents(Set<Reagent> reagents) {
        this.reagents = reagents;
    }

    public Method(Long methodId, String methodName) {
        this.methodId = methodId;
        this.methodName = methodName;
    }

    public Method() {
    }

    public Long getMethodId() {
        return methodId;
    }

    public void setMethodId(Long methodId) {
        this.methodId = methodId;
    }

    public String getMethodName() {
        return methodName;
    }

    public void setMethodName(String methodName) {
        this.methodName = methodName;
    }
}
