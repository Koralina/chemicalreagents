package com.chem.chemistry.model;

import javax.persistence.*;
import java.util.Set;

@Entity
public class Reagent {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    private Long reagentId;
    private String name;

    @ManyToMany(mappedBy = "reagents")
    private Set<Method> methods;

    // List<String> sortedNames = names.stream().sorted().collect(Collectors.toList());

    public Reagent() {
    }

    public Set<Method> getMethods() {
        return methods;
    }

    public void setMethods(Set<Method> methods) {
        this.methods = methods;
    }

    public Reagent(String name) {
        this.name = name;
    }

    public Long getReagentId() {
        return reagentId;
    }

    public void setReagentId(Long reagentId) {
        this.reagentId = reagentId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
