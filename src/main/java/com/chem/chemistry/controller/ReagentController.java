package com.chem.chemistry.controller;
import com.chem.chemistry.model.Method;
import com.chem.chemistry.model.Reagent;
import com.chem.chemistry.service.MethodService;
import com.chem.chemistry.service.ReagentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.HashSet;
import java.util.List;
import java.util.Objects;
import java.util.Set;

@RestController
@RequestMapping("/reagents")
public class ReagentController {
    private ReagentService reagentService;
    private MethodService methodService;

    @Autowired
    public ReagentController(ReagentService reagentService, MethodService methodService) {
        this.reagentService = Objects.requireNonNull(reagentService, "Reagent service must be defined");
        this.methodService = Objects.requireNonNull(methodService, "Method service must be defined");
    }

    @GetMapping
    public ModelAndView addReagentForm() {
        return new ModelAndView("reagents-list")
                .addObject("reagent", new Reagent())
                .addObject("methods", methodService.getAll())
                .addObject("reagents", reagentService.getAll());
    }

    @PostMapping
    public ModelAndView save(@ModelAttribute Reagent reagent, @RequestParam("idChecked") List<Long> methodsId) {
        Set<Method> methods = new HashSet<>();
        for (Long id : methodsId) {
            methods.add(methodService.getById(id).get());
        }
       reagent.setMethods(methods);
        reagentService.save(reagent);

        return new ModelAndView("reagents-list")
                .addObject("reagent", new Reagent())
                .addObject("methods", methodService.getAll())
                .addObject("reagents", reagentService.getAll());
    }
}