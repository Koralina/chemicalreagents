package com.chem.chemistry.controller;
import com.chem.chemistry.model.ReagentOnStock;
import com.chem.chemistry.service.ReagentOnStockService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import java.util.Objects;

@RestController
@RequestMapping("/reagentsonstock")
public class ReagentOnStockController {
    private ReagentOnStockService reagentOnStockService;

    @Autowired
    public ReagentOnStockController(ReagentOnStockService reagentOnStockService) {
        this.reagentOnStockService = Objects.requireNonNull(reagentOnStockService, "Reagent on stock service must be defined");
    }

    @GetMapping
    public ModelAndView addReagentOnStockForm (){
        return new ModelAndView("reagentsOnStock-list")
                .addObject("reagentOnStock", new ReagentOnStock())
                .addObject("reagentsOnStock", reagentOnStockService.getAll());
    }

    @PostMapping
    public ModelAndView save(@ModelAttribute ReagentOnStock reagentOnStock) {
        reagentOnStockService.save(reagentOnStock);

        return new ModelAndView("reagentsOnStock-list")
                .addObject("reagentOnStock", new ReagentOnStock())
                .addObject("reagentsOnStock", reagentOnStockService.getAll());
    }

//    @PostMapping
//    public ModelAndView deleteReagentFromStock(@PathVariable Long id) {
//        reagentOnStockService.delete(id);
//        return ModelAndView("reagentsOnStock-list")
//    }
}