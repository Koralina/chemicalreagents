package com.chem.chemistry.controller;

import com.chem.chemistry.model.Method;
import com.chem.chemistry.model.Reagent;
import com.chem.chemistry.service.MethodService;
import com.chem.chemistry.service.ReagentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;

import java.util.*;

@RestController
@RequestMapping("/methods")
public class MethodController {

    private MethodService methodService;
    private ReagentService reagentService;


//    public MethodController(MethodService methodService) {
//        this.methodService = Objects.requireNonNull(methodService, "Method service must be defined");}

    @Autowired
    public MethodController(MethodService methodService, ReagentService reagentService) {
        this.methodService = Objects.requireNonNull(methodService, "Method service must be defined");
        this.reagentService = Objects.requireNonNull(reagentService, "Reagent service must be defined");
    }

    @GetMapping
    public ModelAndView addMethodForm() {
        return new ModelAndView("methods-list")
                .addObject("method", new Method())
                .addObject("reagents", reagentService.getAll())
                .addObject("methods", methodService.getAll());
    }

    @PostMapping
    public ModelAndView save(@ModelAttribute Method method,
                             @RequestParam(required = false, value = "idChecked") List<Long> reagentsId) {
        if (Objects.nonNull(reagentsId)) {
            Set<Reagent> reagents = new HashSet<>();
            for (Long id : reagentsId) {
                reagents.add(reagentService.getById(id).get());
            }
            method.setReagents(reagents);
        }
        methodService.save(method);

        return new ModelAndView("methods-list")
                .addObject("method", new Method())
                .addObject("reagents", reagentService.getAll())
                .addObject("methods", methodService.getAll());
    }

//    @PostMapping
//    public ModelAndView save(@ModelAttribute Method method) {
//        methodService.save(method);
//
//        return new ModelAndView("methods-list")
//                .addObject("method", new Method())
//                .addObject("methods", methodService.getAll());
//    }

    @RequestMapping("/methods-with-reagents")
    public ModelAndView showMethodsWithReagents(@ModelAttribute Method method, Reagent reagent) {
        return new ModelAndView("methods-with-reagents-list")
                .addObject("reagents", reagentService.getAll())
                .addObject("methods", methodService.getAll());
    }
}
