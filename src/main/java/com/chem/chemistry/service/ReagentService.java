package com.chem.chemistry.service;
import com.chem.chemistry.model.Reagent;
import com.chem.chemistry.repository.ReagentRepository;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class ReagentService {
    private ReagentRepository repository;

    public ReagentService(ReagentRepository repository) {
        this.repository = Objects.requireNonNull(repository, "Reagent repository must be defined");
    }

    public void save(Reagent reagent){
        repository.save(reagent);
    }

    public List<Reagent> getAll() {
        return repository.findAll();

    }

    public Optional<Reagent> getById(Long id) {
        return repository.findById(id);
    }
}