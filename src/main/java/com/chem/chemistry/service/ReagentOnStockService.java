package com.chem.chemistry.service;

import com.chem.chemistry.model.ReagentOnStock;
import com.chem.chemistry.repository.ReagentOnStockRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class ReagentOnStockService {
    private ReagentOnStockRepository repository;
    private List<ReagentOnStock> reagentsOnStock = new ArrayList<>();

    public ReagentOnStockService(ReagentOnStockRepository repository) {
        this.repository = Objects.requireNonNull(repository, "Reagent on stock repository must be defined");
    }

    public void save(ReagentOnStock reagentOnStock){
        repository.save(reagentOnStock);
    }

    public List<ReagentOnStock> getAll() {
        return repository.findAll();

    }
//    public void delete(Long id){
//        repository.deleteById(id);
//    }
}
