package com.chem.chemistry.service;
import com.chem.chemistry.model.Method;
import com.chem.chemistry.repository.MethodRepository;
import org.springframework.stereotype.Service;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.Optional;

@Service
public class MethodService {
    private MethodRepository repository;
//    private List<Method> methods = new ArrayList<>();
    public MethodService(MethodRepository repository) {
        this.repository = Objects.requireNonNull(repository, "Method repository must be defined");
    }
    public void save(Method method){
        repository.save(method);
    }

    public List<Method> getAll() {
        return repository.findAll();
    }
    public Optional<Method> getById(Long id) {
        return repository.findById(id);
    }
}
