package com.chem.chemistry.repository;

import com.chem.chemistry.model.Reagent;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ReagentRepository extends CrudRepository<Reagent, Long> {

    List<Reagent> findAll();
}
