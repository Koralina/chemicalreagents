package com.chem.chemistry.repository;
import com.chem.chemistry.model.Method;
import org.springframework.data.repository.CrudRepository;
import java.util.List;

public interface MethodRepository extends CrudRepository <Method, Long> {
    List<Method> findAll();
}
