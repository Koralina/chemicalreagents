package com.chem.chemistry.repository;

import com.chem.chemistry.model.ReagentOnStock;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface ReagentOnStockRepository extends CrudRepository <ReagentOnStock, Long> {
    List<ReagentOnStock> findAll();
}
